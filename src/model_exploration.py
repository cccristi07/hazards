import numpy as np
import cv2
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
# from skimage.feature import hog
from sklearn.decomposition import PCA
from sklearn.preprocessing import normalize, scale
from skimage.feature import hog
import os
import matplotlib.pyplot as plt
import pickle
from sklearn.externals import joblib
from sklearn.tree import DecisionTreeClassifier
from src.utils import *
from sklearn.metrics import classification_report
from sklearn.naive_bayes import  GaussianNB
import json

n_bins = [4,8,16,32,64]
color_wise = [False]



dataset_path = 'C:/Users/Cristian/Desktop/hazarde/fire'
positive_imgs = [f for f in os.listdir(dataset_path + "/1")]
negative_imgs = [f for f in os.listdir(dataset_path + "/0")]


for bin in n_bins:

    for color in color_wise:

        print("BIN = {} COLOR = {}".format(bin, color))
        # construim datasetul
        X_train = []  # vector de features
        y_train = []  # vector de labels

        X_test = []
        y_test = []

        train_percent = 0.7

        REBUILD_DATASET=True

        if not REBUILD_DATASET:
            print("Loading dataset")
            try:

                X_train = np.load(dataset_path + "/X_train.npy", X_train)
                X_test = np.load(dataset_path + "/X_test.npy", X_test)
                y_train = np.load(dataset_path + "/y_train.npy", y_train)
                y_test = np.load(dataset_path + "/y_test.npy", y_test)
            except:
                print("Files not found!!!")

        else:

            print("Building training set")

            poz_index = int(len(positive_imgs) * train_percent)
            for path in positive_imgs[:poz_index]:
                img_path = dataset_path + "/1/" + path

                try:
                    X_train.append(get_features(img_path, bin, color))
                    y_train.append(1)
                except Exception as e:
                    raise(e)

            negative_index = int(len(negative_imgs) * train_percent)
            for path in negative_imgs[:negative_index]:
                img_path = dataset_path + "/0/" + path
                try:
                    X_train.append(get_features(img_path, bin, color))
                    y_train.append(0)

                except Exception:
                    print("SOME ERROR OCCURED")

            print("Building test set")
            for path in positive_imgs[poz_index:]:
                img_path = dataset_path + "/1/" + path

                try:
                    X_test.append(get_features(img_path, bin, color))
                    y_test.append(1)

                except Exception as e:
                    raise(e)

            for path in negative_imgs[negative_index:]:
                img_path = dataset_path + "/0/" + path

                try:
                    X_test.append(get_features(img_path, bin, color))
                    y_test.append(0)

                except Exception as e:
                    raise(e)

            np.save(dataset_path + "/X_train_BINS{}_COLOR{}".format(bin, color), X_train)
            np.save(dataset_path + "/X_test_BINS{}_COLOR{}".format(bin, color), X_test)
            np.save(dataset_path + "/y_train_BINS{}_COLOR{}".format(bin, color), y_train)
            np.save(dataset_path + "/y_test_BINS{}_COLOR{}".format(bin, color), y_test)

            X_train = np.array(X_train)
            X_test = np.array(X_test)
            y_train = np.array(y_train)
            y_test = np.array(y_test)

        # shuffle the data
        shuffle = list(range(0, len(y_train)))
        np.random.shuffle(shuffle)
        X_train = X_train[shuffle]
        y_train = y_train[shuffle]

        print("X_TRAIN SHAPE {}".format(X_train.shape))
        # X_train = normalize(X_train)
        y_train = np.squeeze(y_train)
        y_test =  np.squeeze(y_test)



        X_train = scale(X_train)
        X_test = scale(X_test)


        nb = GaussianNB()
        lgr = LogisticRegression(C=0.5, class_weight='balanced', verbose=False, )
        svm = SVC(C=1, kernel='rbf', verbose=False, class_weight='balanced', )
        tree = DecisionTreeClassifier()


        nb.fit(X_train, y_train)
        svm.fit(X_train, y_train)
        lgr.fit(X_train, y_train)
        tree.fit(X_train, y_train)
        clfs = [(nb, 'NaiveBayes'), (svm, 'SVM'), (lgr, 'Logistic Regression'), (tree,'DecisionTree')]

        for clf, name in clfs:

            score = clf.score(X_test, y_test)
            y_pred = clf.predict(X_test)

            print(name, " CLF REPORT:")
            print(classification_report(y_test, y_pred))
            print("Mean accuracy for {} is {}".format(name, score))




        # print("Tree test acc ", tree.score(X_test, y_test))
        #
        #
        #
        # y_pred = lgr.predict(X_test)
        #
        # # print(classification_report(y_test, y_pred, ["FLOOD", "NO_FLOOD"]))
        # test_score = lgr.score(X_test,y_test)
        # print("Logistic Regression")
        # print(classification_report(y_test, y_pred))
        # print("LOGREG test score ", test_score)
        #
        # print("Training SVM")
        #
        # test_score = svm.score(X_test, y_test)
        # print("SVM test score ", test_score)
        #
        # print("Naive bayes score ", nb.score(X_test,y_test))
        # y_pred = nb.predict(X_test)
        # print("Naive bayes report", classification_report(y_test, y_pred))





        # import matplotlib.image as mpimg
        # for dirpath, _, filenames in os.walk(dataset_path + "/img_test/"):
        #     img_features = []
        #     for file in filenames:
        #         print(file)
        #         img_path = os.path.abspath(os.path.join(dirpath, file))
        #         img_features.append(get_features(img_path, bin, color))
        #
        #
        #     img_features = scale(img_features)
        #     predictions = nb.predict(img_features)
        #     for index, file in enumerate(filenames):
        #         img_path = os.path.abspath(os.path.join(dirpath, file))
        #         img = mpimg.imread(img_path)
        #         plt.imshow(img)
        #         plt.title("Prediction outside: {}".format(predictions[index]))
        #         plt.show()
        #
        # input("STOPHERE")
