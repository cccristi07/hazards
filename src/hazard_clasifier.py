import os
import matplotlib.pyplot as plt
import json
import pickle

#clasificatoare
from sklearn.svm import SVC
from sklearn.naive_bayes import  GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.decomposition import PCA
from sklearn.linear_model import LogisticRegression

#utilitati
from sklearn.metrics import classification_report
from sklearn.externals import joblib
from sklearn.preprocessing import normalize, scale
from src.utils import get_features

#calcul numeric
import numpy as np




class HazardClassifier():
    '''
       Clasa folosita ca Wrapper pentru solutia de clasificare

    '''

    def __init__(self, model=None, dataset_path=None, n_bins=4, color=False):
        '''

        :param model: modelul incarcat
        :param dataset_path: calea catre datasest
        :param n_bins: param pentru extragerea de caracteristici
        :param color: rgb v. greyscale
        '''
        self.model = model
        self.dataset = dataset_path
        self.n_bins = n_bins
        self.color= color
        self.data = None

        self.X_train = None
        self.X_test = None
        self.y_train = None
        self.y_test = None


    def _get_images(self):
        # construieste dataset din imagini
        X_train = []  # vector de features
        y_train = []  # vector de labels

        X_test = []
        y_test = []

        train_percent = 0.7

        positive_imgs = [f for f in os.listdir(self.dataset + "/1")]
        negative_imgs = [f for f in os.listdir(self.dataset+ "/0")]
        self.positive_imgs = positive_imgs
        self.negative_imgs = negative_imgs


        bin = self.n_bins
        color = self.color
        print("Building training set")

        poz_index = int(len(positive_imgs) * train_percent)
        self.poz_index = poz_index
        for path in positive_imgs[:poz_index]:
            img_path = self.dataset + "/1/" + path

            try:
                X_train.append(get_features(img_path, bin, color))
                y_train.append(1)
            except Exception as e:
                raise (e)

        neg_index = int(len(negative_imgs) * train_percent)
        self.neg_index = neg_index
        for path in negative_imgs[:neg_index]:
            img_path = self.dataset + "/0/" + path
            try:
                X_train.append(get_features(img_path, bin, color))
                y_train.append(0)

            except Exception as e:
                raise e

        print("Building test set")
        for path in positive_imgs[poz_index:]:
            img_path = self.dataset + "/1/" + path

            try:
                X_test.append(get_features(img_path, bin, color))
                y_test.append(1)

            except Exception as e:
                raise (e)

        for path in negative_imgs[neg_index:]:
            img_path = self.dataset + "/0/" + path

            try:
                X_test.append(get_features(img_path, bin, color))
                y_test.append(0)

            except Exception as e:
                raise (e)


        X_train = np.array(X_train)
        X_test = np.array(X_test)
        y_train = np.array(y_train)
        y_test = np.array(y_test)

        # shuffle the data
        shuffle = list(range(0, len(y_train)))
        np.random.shuffle(shuffle)
        X_train = X_train[shuffle]
        y_train = y_train[shuffle]

        y_train = np.squeeze(y_train)
        y_test =  np.squeeze(y_test)

        X_train = scale(X_train)
        X_test = scale(X_test)



        self.X_train = X_train
        self.X_test = X_test
        self.y_train = y_train
        self.y_test = y_test

        self.data = dict(
            X_train = X_train,
            X_test = X_test,
            y_test = y_test,
            y_train = y_train
        )


        return self.data

    def save_dataset(self, path):
        '''

        :param path: calea de salvare a datasetului transformat
        :return: selr
        '''
        #salveaza dataset
        json_data = dict(
            X_train = self.data["X_train"].tolist(),
            y_train=self.data["y_train"].tolist(),
            X_test=self.data["X_test"].tolist(),
            y_test=self.data["y_test"].tolist(),
        )

        json.dump(json_data, open(path, "w"))


    def load_dataset(self, path):
        '''

        :param path: calea de unde se va incarca datasetul
        :return:
        '''
        #incarca dataset
        with open(path, 'r') as f:
            json_str = f.read()

        self.data = json.loads(json_str)
        self.data = {key: np.array(value) for key, value in self.data.items()}
        self.X_train = self.data["X_train"]
        self.X_test =  self.data["X_test"]
        self.y_train =  self.data["y_train"]
        self.y_test =  self.data["X_test"]



    def train(self):
        if self.data is None:
            self._get_images()
        print("Fitting model")
        self.model.fit(self.X_train, self.y_train)


    def load_model(self, model_path):
        '''

        :param model_path: calea de unde se va incarca modelul
        :return:
        '''
        self.model = joblib.load(model_path)

    def save_model(self, model_path):
        '''

        :param model_path: calea unde se va salva modelul
        :return:
        '''
        joblib.dump(self.model, model_path)

    def get_scores(self):
        '''

        :return:
        '''
        test_acc = self.model.score(self.X_test, self.y_test)
        y_pred = self.model.predict(self.X_test)
        print("Report ")
        print(classification_report(self.y_test, y_pred))
        print("Test accuracy is ", test_acc)

    def dimensionality_reduction(self, n_comp):
        '''
        metoda folosita pentru imbunatatirea features, nu mai e folosita momentan
        :param n_comp:
        :return:
        '''
        pca = PCA(n_components=n_comp, )
        self.X_train = pca.fit_transform(self.X_train)
        self.X_test = pca.fit_transform(self.X_test)

    def predict(self, img_path):
        features = [get_features(img_path, self.n_bins, self.color)]
        features = scale(features)

        return self.model.predict(features)


    def demo(self,test_dir):
        '''

        :param test_dir: calea unde se afla imaginile de test
        :return:
        '''
        import matplotlib.image as mpimg
        for dirpath, _, filenames in os.walk(test_dir):
            img_features = []
            for file in filenames:
                # obtinem caracteristicile
                img_path = os.path.abspath(os.path.join(dirpath, file))
                img_features.append(get_features(img_path, self.n_bins, self.color))


            img_features = scale(img_features)

            # obtinem predictia
            predictions = self.model.predict(img_features)
            for index, file in enumerate(filenames):
                img_path = os.path.abspath(os.path.join(dirpath, file))
                img = mpimg.imread(img_path)
                plt.imshow(img)
                plt.title("Prediction in class: {}".format(predictions[index]))
                plt.show()






# testare 
if __name__ == '__main__':
    fire_dataset_path = "C:/Users/Cristian/Desktop/hazarde/fire"
    fire = HazardClassifier(SVC(C=1.5, kernel='rbf', verbose=False, class_weight='balanced', ),
                            fire_dataset_path, n_bins=64)
    fire.train()
    fire.get_scores()
    fire.load_dataset("fire_model_dataset_F64")
    fire.load_model("fire_model_SVM_F64")
    fire.demo(fire_dataset_path + "/img_test/")

    flood_dataset = 'C:/Users/Cristian/Desktop/hazarde/floods'

    flood = HazardClassifier(LogisticRegression(C=1.5, class_weight='balanced'), dataset_path=flood_dataset,n_bins=4)
    flood.train()
    flood.get_scores()
    flood.save_model('flood_model')
    flood.save_dataset('flood_dataset')
    flood.demo(flood_dataset + '/img_test/')