import cv2
import numpy as np
from skimage.feature import hog
from skimage.exposure import histogram

# extragere caracteristici imagine
def get_features(img_path, n_bins=16, color_wise=False):
    '''
      functie care extrage caracteristicile HOG

    :param img_path: calea catre imagine
    :param n_bins: numarul de bucket-uri pentru unghiuri
    :param color_wise: imagine color sau greyscale
    :return: vector cu caracteristicile HOG
    '''


    img = cv2.imread(img_path)
    img = np.array(img / 256.0, np.float32)
    hist, _ = histogram(img[:,:,:])
    hist = [np.mean(hist), np.std(hist)]




    if color_wise:
        img.resize((200,100,3))
        features = hog(img, orientations=n_bins, pixels_per_cell=(50,50), cells_per_block=(1,1),block_norm='L2',
                       multichannel=True)
    else:
        img.resize((200,100))
        features = hog(img, orientations=n_bins, pixels_per_cell=(50,50), cells_per_block=(1,1),block_norm='L2')


    return np.concatenate((features, hist)).flatten()

